# Aufsetzen der Lösung

### Allgemeine Voraussetzungen
Um die Lösung in einem Netzwerk aufzusetzen und zu reproduzieren, werden zunächst zwei Raspberry Pis (Generation nach Wahl, empfohlen 4) benötigt. Auf diesen ist das Raspberry Pi OS mit dem Raspberry Pi Imager zu installieren. Sobald das Gerät gestartet und hochgefahren ist, kann mit der Installation von Docker begonnen werden. Ist diese Installation abgeschlossen, empfehlen wir, Portainer zur vereinfachten Verwaltung der Container-Umgebung zu deployen. Sobald Portainer bereit ist, kann dessen Web-App einfach über den Browser erreicht werden, um weitere Container zu erstellen (Standard-Port ```9000```). 

### Aufsetzen eines Main Devices

An dieser Stelle ist es wichtig zu unterscheiden, ob ein **Main oder Secondary Device** erstellt werden soll. Abhängig davon ist bei einem **Main Device** ein zusätzlicher Container als MQTT-Broker zu erstellen. In unserem MVP haben wir uns für den Mosquitto-Broker entschieden, da die lokale Version des HiveMQ-Brokers Fehlermeldungen ausgegeben hat. Für den Broker ist zu empfehlen, die Standard-Ports zu verwenden, um im Nachgang weniger Anpassungen in der von uns bereits erstellten Node-RED Umgebung vornehmen zu müssen. Ist dieser erfolgreich gestartet, kann mit dem Deployment der Node-RED Umgebung begonnen werden. 

### Aufsetzen eines Main und Secondary Devices

Dies ist sowohl für **Main und Secondary Device** zu tätigen. Hierfür ist zu Beginn in Portainer eine neue Volume, bspw. mit dem Namen _„node_red_data“_ zu erzeugen (Gilt für jeden Node-RED Container). Anschließend kann unter Container ein neuer Container erstellt werden. Dabei ist neben einem aussagekräftigen Namen und dem Image (```nodered/node-red:latest```) im Bereich Advanced Container Settings, unter dem Reiter Volumes, das zuvor erstellte Volume zu mappen. Innerhalb des Containers ist dabei auf _„/data“_ zu referenzieren. Zusätzlich kann als Environment Variable _„TZ“_ auf _„Europe/Berlin“_ und als Restart Policy _„Unless stopped“_ angegeben werden, bevor der Container erstellt wird. Sollten mehrere Node-RED Container auf demselben Pi betrieben werden, ist im Bereich Network Ports Configuration unter Manual Network Port Publishing ein freier Port des Hosts (bspw. ```1881```) auf den Port ```1880``` des Containers zu mappen. Nun kann der Container deployed werden. 
Sobald Node-RED einsatzbereit ist, kann die Web-App standardmäßig über den Port ```1880``` erreicht werden. Hat das Interface geladen, können über das Burger-Symbol Flows importiert werden. An dieser Stelle ist die JSON-File aus diesem [Repository](https://gitlab.reutlingen-university.de/iot-team3/iot-test/-/tree/main/) einzufügen. Anschließend erscheinen zwei neue Flows: _„IoT Hackathon Publisher“_ (unrelevant bei Secondary Device, kann entfernt werden) und _„IoT Hackathon Subscriber“_ in der Umgebung. Wie in den Absätzen zuvor beschrieben, vereinen die jeweiligen Flows Backend und Frontend der Anwendung. Um die Lösung einsatzfähig zu machen, ist an Nodes in beiden Flows mit einem _„!“_ eine Anpassung der IP-Adresse (```10.0.100.132```) auf die eigene/auf das Main Device vorzunehmen (Abhängig davon, ob es sich um eine MQTT-Node handelt). Gleiches gilt, wenn die Node-RED Umgebung nicht unter dem Port ```1880``` betrieben wird. Sind alle Änderungen vorgenommen, kann in der rechten, oberen Ecke die Lösung deployed werden und ist einsatzbereit.

### Übersicht der Schnittstellen

Wurde ein **Secondary Device** erstellt, sind folgende Adressen über den Browser zu erreichen:```{IP-Adresse:Port}```
- ```/reactor``` [HTTP GET], sucht nach neuen Interactoren, routet bei Erfolg auf ```/reactor/umleitung?uuid={Interactor UUID}```
- ```/reactor/topic``` [HTTP GET], liefert Topic, unter dem ausgewählter Intactor publiziert, ```Content-Type: application/json```
- ```/reactor/name``` [HTTP GET], liefert angegebenen Namen von ausgewähltem Interactor, ```Content-Type: application/json```
- ```/reactor/umleitung?uuid={Interactor UUID}``` [HTTP GET], liefert Webpage zur Darstellung der Reaktionen eines Interactors, ```Content-Type: text/html```
- ```/reactor/reaction?uuid={Interactor UUID}``` [HTTP GET], liefert zuletzt gesendete Reaktion(en) eines Interactors, ```Content-Type: application/json```
- ```/reactor/files/script.js``` [HTTP GET], liefert Javascript für ```/reactor/umleitung?uuid={Interactor UUID}```, ```Content-Type: application/javascript``` 
- ```/reactor/files/stylesheet.css``` [HTTP GET], liefert CSS für ```/reactor/umleitung?uuid={Interactor UUID}```, ```Content-Type: text/css``` 

Wurde ein **Main Device** erstellt, sind **zusätzlich** weitere Adressen verfügbar:```{IP-Adresse:Port}```
- ```/interactor``` [HTTP GET], liefert Webpage zum Senden der Reaktionen, ```Content-Type: text/html```
- ```/interactor/files/script.js``` [HTTP GET], liefert Javascript für ```/interactor```, ```Content-Type: application/javascript``` 
- ```/interactor/files/stylesheet.css``` [HTTP GET], liefert CSS für ```/interactor```, ```Content-Type: text/css```
- ```/interactor/reactions``` [HTTP POST], nimmt zu veröffentlichende Reaktionen unter Topic ```IoTHackathonHHZ/Reaktionen/{UUID}``` an, ```Content-Type: application/json```, benötigt Parameter ```user``` und ```reaction```

### Optional: autom. startup Main Device

Um sich redundante Schritte nach jedem Start des Raspis zu ersparen und **direkt als Reactor empfangsbereit zu sein**, kann zusätzlich folgende Einstellung vorgenommen werden (Chromium startet anschließend automatisch im Kiosk Modus nach dem Start):

Bearbeite die autostart Datei des Raspis
```cmd
sudo nano /etc/xdg/lxsession/LXDE-pi/autostart 
```
Fügen den folgenden Code ein, wobei ```YOUR_LOCAL_IP``` die Netzwerkadresse des Pi und ```YOUR_PORT``` der exponierte Port des Node Red Containers ist: 
```cmd
@xset s off
@xset -dpms
@xset s noblank
@chromium-browser --kiosk http://$(YOUR_LOCAL_IP):$(YOUR_PORT)/reactor
```

Um den Kiosk-Modus auf dem RasPi zu beenden, ```Alt + F4``` drücken.
